def call(String repo, String image_name) {
    sh "docker build . -t $repo:$image_name"
    withCredentials([usernamePassword(credentialsId: "docker-hub-repo",usernameVariable: "USER", passwordVariable: "PASS")]) {
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push $repo:$image_name"
    }
}

return this